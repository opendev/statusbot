===================================
OpenStack Infrastructure Status Bot
===================================

statusbot is the bot that the OpenStack Infra team uses to alert
the developer community about issues and resolutions related to
the health of the developer infrastructure.
